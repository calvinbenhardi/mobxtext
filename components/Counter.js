import React, { Component } from "react";
import { Button, View, Text, StyleSheet } from "react-native";
import { observer, inject } from "mobx-react";
import store from 'react-native-simple-store';

@inject("counterStore")
@observer
export default class Counter extends Component {
  constructor(props){
    super(props);
    this.state = {
      counter : '',
    }
  }

  getData(){
    store.get('counter').then((res) => {
      this.setState({counter:res.total})
    });
    console.log("Data : "+this.state.counter);
  }



  render() {
    return (
      <View style={Styles.container}>
        <Button
          title="Show Store"
          onPress={() => this.getData()}
        />
        <Text>Current Count: {this.props.counterStore.count}</Text>
        <Text>Last Count: {this.state.counter}</Text>
      </View>
    );
  }
}

let Styles = StyleSheet.create({
  container: {
    padding: 15
  }
});
