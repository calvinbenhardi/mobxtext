import { observable, action } from "mobx";
import store from 'react-native-simple-store';

export default class CounterStore {
  @observable count = 0;

  @action increase() {
    this.count += 1;

    store.save('counter', {
  		total: this.count
  	})

    store.get('counter')
      .then((res) =>
      	console.log(res.total) // 'Blurry Face'
      )

  }

  @action decrease() {
    this.count -= 1;
  }
}
